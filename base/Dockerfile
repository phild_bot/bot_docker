ARG ROS2_VERSION=humble
ARG BASE_IMAGE=dustynv/ros:${ROS2_VERSION}-ros-base-l4t-r32.7.1

FROM ${BASE_IMAGE} AS developer
ENV DEBIAN_FRONTEND=noninteractive
ARG USERNAME=ros
ARG USER_UID=1000
ARG USER_GID=${USER_UID}

# The development environment add build dependencies here
RUN apt update && apt install -y --no-install-recommends \
    libgtk2.0-dev \
    libva-dev \
    libvdpau-dev \
    valgrind \
    kcachegrind \
    libfreeimage-dev \
    libmetis-dev \
    libglew-dev \
    libcgal-dev \
    git-lfs \
    clang-format \
    clang-tidy \
    unzip \
    bash-completion \
    build-essential \
    cmake \
    gdb \
    git \
    openssh-client \
    python3-argcomplete \
    python3-pip \
    nano \
    environment-modules \
    libtbb-dev \
    rsync \
    && rm -rf /var/lib/apt/lists/*
RUN add-apt-repository ppa:ubuntu-toolchain-r/test && apt install gcc-9 g++-9 -y && \
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 9 --slave /usr/bin/g++ g++ /usr/bin/g++-9


ENV GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
WORKDIR /opt
COPY install_fmt.sh .
RUN git clone --branch 10.2.1 https://github.com/fmtlib/fmt.git &&\
    mkdir -p fmt/build &&\
    cd fmt/build &&\
    cmake .. &&\
    make && make install

RUN pip install Jinja2 typeguard


ARG USERNAME=ros
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create a non-root user
RUN groupadd --gid $USER_GID ${USERNAME} \
    && useradd -s /bin/bash --uid ${USER_UID} --gid ${USER_GID} -m ${USERNAME}
# Add sudo support for the non-root user
#&& apt-get update \
#&& apt-get install -y sudo \
#&& echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME\
#&& chmod 0440 /etc/sudoers.d/$USERNAME \
#&& rm -rf /var/lib/apt/lists/*

RUN usermod -aG dialout ${USERNAME}
