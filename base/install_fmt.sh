#!/bin/bash

git clone --branch 10.2.1 https://github.com/fmtlib/fmt.git
mkdir -p fmt/build
cd fmt/build
cmake ..
make && make install
