#!/bin/bash

docker build base -t phildue/bot:base
docker build ros2_control -t phildue/bot:base_ros2_control
docker build bot_base --no-cache -t phildue/bot:bot_base
docker build bot_ui --no-cache -t phildue/bot:bot_ui