# bot_docker

Collection of scripts to create docker images for bot.

- [base](base/Dockerfile): Creates the basic docker image with ROS and some basic dependencies.
- [ros2_control](ros2_control/Dockerfile): A docker image containing the ros2 control packages.
- [bot_base](bot_base/Dockerfile): A docker image containing [bot_base](https://gitlab.com/phild_bot/bot_base).
- [bot_ui](bot_ui/Dockerfile): A docker image containing [bot_ui](https://gitlab.com/phild_bot/bot_ui).


Flow Diagram:

```mermaid
graph TD;
    base-->ros2_control;
    ros2_control-->bot_base;
    base-->bot_ui;
```

The images are deployed within [bot_docker_compose](https://gitlab.com/phild_bot/bot_docker_compose).